module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  env: {
    development: {
      // babel-plugin-dynamic-import-node plugin only does one thing by converting all import() to require().
      plugins: ['dynamic-import-node']
    }
  }
};
