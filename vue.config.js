/*
 * @Author: fjt
 * @Date: 2021-07-07 21:25:33
 * @LastEditors: fjt
 * @LastEditTime: 2021-07-20 23:12:10
 */
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  lintOnSave: false,
  pages: {
    index: {
      entry: './src/main.js',
      template: './public/index.html',
      filename: 'index.html',
      title: 'leadshop',
      chunks: ['chunk-leadshop', 'chunk-commom', 'chunk-echarts', 'chunk-elementUi', 'index']
    }
  },
  devServer: {
    open: true,
    overlay: {
      warnings: true,
      errors: true
    }
  },
  publicPath: process.env.NODE_ENV === 'production' ? '/assets/admin' : '/',
  filenameHashing: true,
  productionSourceMap: false,
  configureWebpack: config => {
    config.optimization.splitChunks = {
      chunks: 'all',
      maxInitialRequests: 4,
      cacheGroups: {
        common: {
          name: 'chunk-commom',
          minChunks: 2,
          priority: -20,
          chunks: 'initial',
          reuseExistingChunk: true
        },
        leadshop: {
          name: 'chunk-leadshop',
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        echarts: {
          name: 'chunk-echarts',
          test: /[\\/]node_modules[\\/]_?echarts(.*)/,
          priority: 10
        },
        elementUi: {
          name: 'chunk-elementUi',
          test: /[\\/]node_modules[\\/]_?element-ui(.*)/,
          priority: 10
        }
      }
    };

    if (process.env.BUNDLE_ANALYZER === 'YES') {
      return {
        plugins: [new BundleAnalyzerPlugin()]
      };
    }
  },
  chainWebpack: config => {
    // 移除 prefetch 插件
    config.plugins.delete('prefetch-index');
    // 移除 preload 插件，避免加载多余的资源
    config.plugins.delete('preload-index');

    config.plugin('define').tap(args => {
      return args;
    });
  }
};
